package main

import (
	"bytes"
	"fmt"
	"log"

	// "github.com/golang/protobuf/proto"
	// "github.com/tidwall/gjson"
	monitoringpb "google.golang.org/genproto/googleapis/monitoring/v3"
	"github.com/golang/protobuf/jsonpb"
	// "github.com/gogo/protobuf/jsonpb"
	"github.com/golang/protobuf/ptypes/duration"
)

// alertPolicy is a wrapper around the AlertPolicy proto to
// ensure JSON marshaling/unmarshaling works correctly.
type alertPolicy struct {
	*monitoringpb.AlertPolicy
}

func (a *alertPolicy) MarshalJSON() ([]byte, error) {
	m := &jsonpb.Marshaler{EmitDefaults: true}
	b := new(bytes.Buffer)
	m.Marshal(b, a)
	return b.Bytes(), nil
}

func (a *alertPolicy) UnmarshalJSON(b []byte) error {
	u := &jsonpb.Unmarshaler{}
	a.AlertPolicy = new(monitoringpb.AlertPolicy)
	return u.Unmarshal(bytes.NewReader(b), a)
}

func main() {
	fmt.Printf("Hello!")

	pData := &alertPolicy{}
	pData.AlertPolicy = &monitoringpb.AlertPolicy {
		DisplayName: "stellar-works-231103",
		Documentation: &monitoringpb.AlertPolicy_Documentation {
			Content: "https://cloud.google.com/monitoring/alerts/using-channels-api#monitoring_alert_create_channel-go",
			MimeType: "text/markdown",
		},
		Conditions: []*monitoringpb.AlertPolicy_Condition {
			&monitoringpb.AlertPolicy_Condition{
				DisplayName: "logging/user/default_appengine_response_status_200",
				Condition: &monitoringpb.AlertPolicy_Condition_ConditionThreshold {
					ConditionThreshold: &monitoringpb.AlertPolicy_Condition_MetricThreshold {
						Filter: "metric.type=\"logging.googleapis.com/user/default_appengine_response_status_200\" resource.type=\"gae_app\"",
						Aggregations: []*monitoringpb.Aggregation {
							&monitoringpb.Aggregation {
								AlignmentPeriod: &duration.Duration {
									Seconds: 60,
								},
								PerSeriesAligner: 11,
								CrossSeriesReducer: 3,
							},
						},
						Comparison: 1,
						Duration: &duration.Duration {
							Seconds: 0,
						},
						ThresholdValue: 2,
						Trigger: &monitoringpb.AlertPolicy_Condition_Trigger {
							Type: &monitoringpb.AlertPolicy_Condition_Trigger_Count {
								Count: 1,
							},
						},
					},
				},
			},
		},
	}

	js, err := pData.MarshalJSON()
	if err != nil {
		log.Panicln(err)
	}

	log.Printf("%s\n", string(js))

// 	js = `{
//   "AlertPolicy": {
//     "displayName": "stellar-works-231103",
//     "documentation": {
//       "content": "https://cloud.google.com/monitoring/alerts/using-channels-api#monitoring_alert_create_channel-go",
//       "mimeType": "text/markdown"
//     },
//     "conditions": [
//       {
//         "displayName": "logging/user/default_appengine_response_status_200",
//         "conditionThreshold": {
//           "filter": "metric.type=\"logging.googleapis.com/user/default_appengine_response_status_200\" resource.type=\"gae_app\"",
//           "aggregations": [
//             {
//               "alignmentPeriod": "60s",
//               "perSeriesAligner": "ALIGN_MAX",
//               "crossSeriesReducer": "REDUCE_MAX"
//             }
//           ],
//           "comparison": "COMPARISON_GT",
//           "thresholdValue": 2,
//           "duration": "0s",
//           "trigger": {
//             "count": 1
//           }
//         }
//       }
//     ]
//   }
// }`

	p, err := loadAlertPolicy([]byte(js))
	if err != nil {
		log.Panicln(err)
	}

	log.Printf("%+v", p)
	log.Println("\n---")
	log.Printf("%d", len(p.Conditions))
	log.Println("\n---")
}

// LoadAlertPolicy :
func loadAlertPolicy(data []byte) (*alertPolicy, error) {
	policy := &alertPolicy{}
	if err := policy.UnmarshalJSON(data); err != nil {
		log.Println("Error occurred!")
		return policy, err
	}

	return policy, nil
}
